TEfficiency*get_Effsum(vector<TString> MCsample,TString year,TString var,float low,float high,TString hlt,bool flag){
	const int num=MCsample.size();
        TH1D*h1[num];TH1D*h2[num];TFile*f1[num];
        TEfficiency*pEff;
	for(int i=0;i<num;i++){
	   if(flag) MCsample[i]=MCsample[i]+"_mjj";
           f1[i]=new TFile("../rootfiles/num_"+hlt+"_"+MCsample[i]+year+".root");
	   if(var.Contains("eta")){
		   h1[i]=(TH1D*)f1[i]->Get(Form("hist_full_"+var+"_%.1f_%.1f",low,high));
		   h2[i]=(TH1D*)f1[i]->Get(Form("hist_n_"+var+"_%.1f_%.1f",low,high));
	   }
	   if(var.Contains("pt")){
		   h1[i]=(TH1D*)f1[i]->Get(Form("hist_full_"+var+"_%.0f_%.0f",low,high));
		   h2[i]=(TH1D*)f1[i]->Get(Form("hist_n_"+var+"_%.0f_%.0f",low,high));
	   }
           if(i!=0) h1[0]->Add(h1[i]);
           if(i!=0) h2[0]->Add(h2[i]);
	}
	if( TEfficiency::CheckConsistency(*h2[0],*h1[0])){
		pEff=new TEfficiency(*h2[0],*h1[0]);
	}
	else{
		pEff=new TEfficiency(*h2[0],*h2[0]);
		for(int j=0;j<h2[0]->GetNbinsX();j++){
			cout<<"numerator "<<h2[0]->GetBinContent(j+1)<<" Denorminator "<<h1[0]->GetBinContent(j+1)<<endl;
		}
	}
	for(int i=0;i<num;i++){
		f1[i]->Close();
	}
        return pEff;
}
TEfficiency*get_Effsum_barrel(vector<TString> MCsample,TString year,TString hlt,bool flag){
	const int num=MCsample.size();
        TH1D*h1[num];TH1D*h2[num];TFile*f1[num];
        TEfficiency*pEff;
	for(int i=0;i<num;i++){
	   if(flag) MCsample[i]=MCsample[i]+"_mjj";
           f1[i]=new TFile("../rootfiles/num_"+hlt+"_"+MCsample[i]+year+".root");
	   h1[i]=(TH1D*)f1[i]->Get("h_pt_d_barrel");
	   h2[i]=(TH1D*)f1[i]->Get("h_pt_n_barrel");
           if(i!=0) h1[0]->Add(h1[i]);
           if(i!=0) h2[0]->Add(h2[i]);
	}
	for(int j=0;j<h2[0]->GetNbinsX();j++){
                if(h2[0]->GetBinContent(j+1)>h1[0]->GetBinContent(j+1))
			h2[0]->SetBinContent(j+1,h1[0]->GetBinContent(j+1));
		cout<<"TEST##### numerator "<<h2[0]->GetBinContent(j+1)<<" Denorminator "<<h1[0]->GetBinContent(j+1)<<endl;
	}
	if( TEfficiency::CheckConsistency(*h2[0],*h1[0])){
		pEff=new TEfficiency(*h2[0],*h1[0]);
		cout<<"PASS!!!"<<endl;
	}
	else{
		pEff=new TEfficiency(*h2[0],*h2[0]);
		for(int j=0;j<h2[0]->GetNbinsX();j++){
			cout<<"TEST!!! numerator "<<h2[0]->GetBinContent(j+1)<<" Denorminator "<<h1[0]->GetBinContent(j+1)<<endl;
		}
	}
	for(int i=0;i<num;i++){
		f1[i]->Close();
	}
        return pEff;
}
void run(TString year,TString hlt,bool flag){
	gStyle->SetPaintTextFormat(".4f");
	vector<TString> MCsample={"GJets_SM","G1Jet","DiPhoton","TTG"};
	vector<float> ptbins={200,220,240,280,320,360,400,450};
	vector<float> etabins={0,0.5,1.0,1.5};
        TString pro="combineMC";
	if(flag) pro=pro+"_mjj";
	const int num1=etabins.size()-1;const int num2=ptbins.size()-1;
        TFile*fout=new TFile("eff_both_"+hlt+"_"+pro+year+".root","recreate");
	TEfficiency* pEff1[num1];
	TEfficiency* pEff2[num2];

	vector<int> fColorsMC={kGreen+1,kCyan,kBlue,kViolet,kGray,kOrange,kPink+6,kMagenta,kRed,kBlack};
        TGraphAsymmErrors *graph2[num2];
	TH2D*heff=new TH2D("h2_eff","",ptbins.size()-1,0,ptbins.size()-1,etabins.size()-1,0,etabins.size()-1);
	TH2D*heff1=new TH2D("h2_eff1","",ptbins.size()-1,&ptbins[0],etabins.size()-1,&etabins[0]);
	cout<<"get efficiency"<<endl;

	TCanvas*c2=new TCanvas("c2","",1600,600);
	TLegend*l2=new TLegend(0.15,0.15,0.38,0.55);
	c2->Divide(2);
	for(int i=0;i<num2;i++){
		pEff2[i] = get_Effsum(MCsample,year,"pt",ptbins[i],ptbins[i+1],hlt,flag);
//		pEff2[i]->SetStatisticOption(TEfficiency::kFCP);
		pEff2[i]->SetStatisticOption(TEfficiency::kFNormal);
		pEff2[i]->SetLineColor(fColorsMC[i]);
		pEff2[i]->SetFillColor(0);
		if(ptbins[i]!=ptbins[num2-1])
			l2->AddEntry(pEff2[i],Form("%.0f<p_{T}^{#gamma}<%.0f",ptbins[i],ptbins[i+1]));
		else
			l2->AddEntry(pEff2[i],Form("p_{T}^{#gamma}>%.0f",ptbins[i]));
		c2->cd(1);
		if(i==0){ 
			pEff2[i]->Draw("APL");
			c2->Update();
			graph2[i] = (TGraphAsymmErrors *)pEff2[i]->GetPaintedGraph();
			graph2[i]->SetMinimum(0.);
			graph2[i]->SetMaximum(1.05);
			pEff2[i]->SetTitle(year+" Efficiency;|#eta|;#epsilon");
			c2->Update();
		}
		else{
			pEff2[i]->Draw("PE same");
			c2->Update();
			graph2[i] = (TGraphAsymmErrors *)pEff2[i]->GetPaintedGraph();
		}
		c2->Update();
		fout->cd();
		graph2[i]->Write(Form("combine_eff_pt_%.0f_%.0f",ptbins[i],ptbins[i+1]));

		const int n1=graph2[0]->GetN();
		double x[n1],y[n1];
		for(int k=0;k<graph2[0]->GetN();k++){
                   x[k]=0;y[k]=0;
                   graph2[i]->GetPoint(k,x[k],y[k]);
                   cout<<"check "<<y[k]<<", error "<<graph2[i]->GetErrorY(k)<<" "<<graph2[i]->GetErrorYhigh(k)<<" "<<graph2[i]->GetErrorYlow(k)<<endl;
                   heff->SetBinContent(i+1,k+1,y[k]);
                   heff->SetBinError(i+1,k+1,graph2[i]->GetErrorY(k));
                   heff1->SetBinContent(i+1,k+1,y[k]);
                   heff1->SetBinError(i+1,k+1,graph2[i]->GetErrorY(k));
		}
	}
	fout->cd();
        heff->Write("combineMC_eff");
	l2->SetBorderSize(0);
	l2->Draw("same");
	for(int i=0;i<heff->GetNbinsX();i++){
		heff->GetXaxis()->SetBinLabel(i+1,Form("%.0f-%.0f",ptbins[i],ptbins[i+1]));
	}
	for(int i=0;i<heff->GetNbinsY();i++){
           heff->GetYaxis()->SetBinLabel(i+1,Form("%.1f-%.1f",etabins[i],etabins[i+1]));
        }
        c2->cd(2);
        gStyle->SetPalette(kPastel);
        gStyle->SetOptStat(0);
        heff->SetTitle(year+" Stat. unc.;p_{T};#eta");
        heff->SetMarkerSize(1.3); 
        heff->Draw("colztext e");
	c2->Update();
//	c2->Print(pro+"_"+hlt+"_"+year+"_pt.pdf");

	TCanvas*c1=new TCanvas("c1","",800,600);
	TLegend*l1=new TLegend(0.5,0.2,0.89,0.65);
        const int n = heff->GetNbinsY();
        TGraphAsymmErrors *graph1[n];
        for(int i=0;i<heff->GetNbinsY();i++){
                graph1[i]=new TGraphAsymmErrors();
                for(int k=0;k<heff->GetNbinsX();k++){
                        cout<<i<<" "<<k+1<<" "<<heff1->GetXaxis()->GetBinCenter(k+1)<<" "<<heff->GetBinContent(k+1,i+1)<<endl;                      
                        graph1[i]->SetPoint(k,heff1->GetXaxis()->GetBinCenter(k+1),heff->GetBinContent(k+1,i+1));
                        graph1[i]->SetPointEXhigh(k,heff1->GetXaxis()->GetBinWidth(k+1)/2);
                        graph1[i]->SetPointEXlow(k,heff1->GetXaxis()->GetBinWidth(k+1)/2);
                        graph1[i]->SetPointEYlow(k,heff->GetBinError(k+1,i+1));
                        if((heff->GetBinContent(k+1,i+1)+heff->GetBinError(k+1,i+1))>1)
                                graph1[i]->SetPointEYhigh(k,(1-heff->GetBinContent(k+1,i+1)));
                        else    
                                graph1[i]->SetPointEYhigh(k,heff->GetBinError(k+1,i+1));
                }               
                l1->AddEntry(graph1[i],Form("%.1f<|#eta|<%.1f",etabins[i],etabins[i+1]));
                graph1[i]->SetLineColor(fColorsMC[i]);
                graph1[i]->SetFillColor(0);
                if(i==0){ 
                        graph1[i]->Draw("AP");
                        graph1[i]->SetMinimum(0.);
                        graph1[i]->SetMaximum(1.05);
			graph1[i]->SetTitle(year+" Efficiency;p_{T};#epsilon");
                }       
                else graph1[i]->Draw("P same");
        }

	l1->SetBorderSize(0);
	l1->Draw("same");
	c1->Update();
//	c1->Print(pro+"_"+hlt+"_"+year+"_eta.pdf");

        TEfficiency*pEff_barrel;
	pEff_barrel = get_Effsum_barrel(MCsample,year,hlt,flag);
        TCanvas*c3=new TCanvas("c3","",800,600);
        c3->cd();
        pEff_barrel->SetFillColor(0);
        pEff_barrel->SetLineColor(kRed);
        pEff_barrel->Draw();
        c3->Update();
        TGraphAsymmErrors *graph3 = (TGraphAsymmErrors *)pEff_barrel->GetPaintedGraph();
        graph3->SetMinimum(0.);
        graph3->SetMaximum(1.05);
        pEff_barrel->SetTitle(year+" MC efficiency;p_{T}^{#gamma};#epsilon");
        
//	draw("combined_MC",pEff_barrel,"pt","barrel", year, hlt);
	fout->cd();
        graph3->Write(pro+"eff");

        c3->Update();
//        c3->Print("barrelEff_"+hlt+"_combineMC_"+year+".pdf");
	fout->Close();

}

int combine_both_draw(){
	vector<TString> hlt={"HLT_IsoMu30","HLT_PFJet200"};
	vector<TString> year={"16","17","18"};
	for(int i=0;i<year.size();i++){
		for(int k=0;k<hlt.size();k++){
			TString hlt_sel; hlt_sel=hlt[k];
			bool flag=0;
			run(year[i],hlt_sel,flag);
		}
	}
	return 1;
}
