TH1D* run(TString hlt,TString sample,TString year){
     TFile*f1=new TFile("eff_"+hlt+"_"+sample+year+".root");
     TH1D*h1;
     TGraphAsymmErrors*graph= (TGraphAsymmErrors*)f1->Get(sample+"eff");
     h1 = new TH1D(sample+"_eff","",graph->GetN(),0,graph->GetN());
     for(int i=0;i<graph->GetN();i++){
             double x,y;
             graph->GetPoint(i,x,y);
             h1->SetBinContent(i+1,y);
             h1->SetBinError(i+1,graph->GetErrorY(i));
     }

     cout<<year<<" "<<sample<<" "<<hlt<<endl;
     for(int i=0;i<h1->GetNbinsX();i++){
//           cout<<h1->GetBinContent(i+1)<<" ";
     }
//     cout<<endl;
     return h1;
}
int cal_syst2(){
	gStyle->SetOptStat(0);
	vector<TString> year={"16","17","18"};
        vector<TString> hlt={"HLT_IsoMu30","HLT_PFJet200"};
        vector<float> ptbins={200,220,240,280,320,360,400,450};
	for(int j=0;j<year.size();j++){
                TFile*fout = new TFile("Syst2_"+year[j]+".root","recreate");
                TFile*fin = new TFile("Syst1_"+year[j]+".root");
                TH1D*syst1 = (TH1D*)fin->Get("syst1");
                TCanvas*c1=new TCanvas("c1","",600,600); 
		TPad*fPads1 = new TPad("pad1", "", 0.00, 0.20, 0.99, 0.99);
		TPad*fPads2 = new TPad("pad2", "", 0.00, 0.00, 0.99, 0.20);
		fPads1->SetFillColor(0);
		fPads1->SetLineColor(0);
		fPads2->SetFillColor(0);
		fPads2->SetLineColor(0);
		fPads1->SetBottomMargin(0.012);
		fPads2->SetTopMargin(0.005);
		fPads2->SetBottomMargin(0.5);
		fPads1->Draw();
		fPads2->Draw();
                TH1D*hsf[2];
		for(int i=0;i<hlt.size();i++){
			TString data;
			if(hlt[i].Contains("PFJet")) data="JetHT";
			else if(hlt[i].Contains("Mu")) data="DoubleMuon";
			TH1D*hdata = run(hlt[i],data,year[j]);
			TH1D*hmc = run(hlt[i],"combineMC",year[j]);
			hsf[i] = (TH1D*)hdata->Clone("sf_"+hlt[i]);
			hsf[i]->Divide(hmc);
		}
		for(int k=0;k<hsf[0]->GetNbinsX();k++){
//                        hsf[1]->SetBinError(k+1,syst1->GetBinContent(k+1)); 
			cout<<hsf[0]->GetBinContent(k+1)<<" "<<hsf[1]->GetBinContent(k+1)<<endl;
		}
		fPads1->cd();
                TLegend*l1=new TLegend(0.12,0.12,0.42,0.42);
                hsf[0]->GetYaxis()->SetRangeUser(0.2,1.1);
                hsf[0]->SetTitle("SF "+year[j]+";;SFs");
                hsf[0]->SetLineColor(2);
                hsf[1]->SetLineColor(3);
                l1->AddEntry(hsf[0],"From DoubleMuon");
                l1->AddEntry(hsf[1],"From JetHT");
		l1->SetBorderSize(0);
                hsf[0]->GetXaxis()->SetLabelSize(0);
                hsf[0]->Draw("PL");
                l1->Draw();
                hsf[1]->Draw("PL same");
                TH1D*hdiff = (TH1D*)hsf[0]->Clone("hdiff");
                hdiff->Add(hsf[1],-1);
                for(int ik=0;ik<hdiff->GetNbinsX();ik++){
                    hdiff->SetBinError(ik+1,0);
                    cout<<"Syst2 "<<abs(hdiff->GetBinContent(ik+1))<<endl;
		}
		fPads1->Update();
		fPads2->cd();
		hdiff->GetYaxis()->SetNdivisions(404);
                hdiff->SetMarkerStyle(20);
                hdiff->SetLineColor(1);
		hdiff->GetYaxis()->SetRangeUser(-0.15,0.18);
                hdiff->Draw("PL");
                hdiff->SetTitle("");
                hdiff->GetYaxis()->SetTitle("Unc");
		hdiff->GetYaxis()->SetTitleOffset(0.3);
                hdiff->GetXaxis()->SetTitle("p_{T}^{#gamma}");
		hdiff->GetXaxis()->SetTitleOffset(0.9);
                hdiff->GetYaxis()->SetTitleSize(0.14);
                hdiff->GetXaxis()->SetTitleSize(0.20);
                hdiff->GetYaxis()->SetLabelSize(0.15);
                hdiff->GetXaxis()->SetLabelSize(0.15);
		fPads2->Update();
		c1->Update();
                c1->Print("syst2_"+year[j]+".pdf");
                fout->cd();
                hdiff->Write("syst2");
                fout->Close();
	}

	return 0;
}
