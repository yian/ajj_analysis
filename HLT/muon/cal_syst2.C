void run(TString data,TString year){
	gStyle->SetPaintTextFormat(".4f");
	TFile*fdata[2];TH2D*hdata[2];TFile*fmc[2];TH2D*hmc[2];
        TH2D*hsf[2];
	vector<TString> hlt;
	if(data.Contains("MET")){
		if(year.Contains("16"))
			hlt = {"HLT_PFMET120_PFMHT120_IDTight","HLT_PFMETNoMu120_PFMHTNoMu120_IDTight"};
		else  hlt={"HLT_PFMET140_PFMHT140_IDTight","HLT_PFMETTypeOne140_PFMHT140_IDTight"};
	}
	else if(data.Contains("JetHT"))
		hlt={"HLT_PFJet200","HLT_PFJet140"};
	else if(data.Contains("DoubleMuon")){
		if(year.Contains("16"))
			hlt={"HLT_IsoMu27","HLT_IsoTkMu27"};
		else
			hlt={"HLT_IsoMu27","HLT_IsoMu30"};
	}
	for(int i=0;i<2;i++){
		TString hlt_sel;hlt_sel=hlt[i];
		fdata[i]=new TFile("eff_target_"+hlt_sel+"_"+data+year+".root");
		fmc[i]=new TFile("eff_target_"+hlt_sel+"_combineMC"+year+".root");
                hdata[i]=(TH2D*)fdata[i]->Get(data+"_eff");
                hmc[i]=(TH2D*)fmc[i]->Get("combineMC_eff");
                hdata[i]->Divide(hmc[i]);
                hsf[i]=(TH2D*)hdata[i]->Clone("sf_"+hlt_sel);
	}
	vector<float> ptbins={200,220,240,280,320,360,400,450};
	if(year.Contains("16")) ptbins={200,220,240,280,320,360,400,450};
        vector<float> etabins={0,0.3,0.6,0.9,1.5};
	TH2D*hsf_syst2=new TH2D("hsf_syst2","",ptbins.size()-1,0,ptbins.size()-1,etabins.size()-1,0,etabins.size()-1);
	for(int i=0;i<hsf_syst2->GetNbinsX();i++){
		hsf_syst2->GetXaxis()->SetBinLabel(i+1,Form("%.0f-%.0f",ptbins[i],ptbins[i+1]));
		for(int j=0;j<hsf_syst2->GetNbinsY();j++){
			hsf_syst2->GetYaxis()->SetBinLabel(j+1,Form("%.1f-%.1f",etabins[j],etabins[j+1]));
			hsf_syst2->SetBinContent(i+1,j+1,abs(hsf[0]->GetBinContent(i+1,j+1)-hsf[1]->GetBinContent(i+1,j+1)));
		}
	} 
        TCanvas*c2=new TCanvas("c2","",800,600);
        gStyle->SetOptStat(0);
        gStyle->SetPalette(kPastel);
        hsf_syst2->SetTitle(year+" SF unc;p_{T};#eta");
        hsf_syst2->SetMarkerSize(1.4);
        hsf_syst2->Draw("colztext");
        c2->Print(data+"_target_SF_syst2_"+year+".pdf");
        TFile*fout=new TFile(data+"_target_SF_syst2_"+year+".root","recreate");
	fout->cd();
	hsf_syst2->Write();
        hsf[0]->Write();
        hsf[1]->Write();
	fout->Close();
	for(int i=0;i<2;i++){
		fdata[i]->Close();
		fmc[i]->Close();
	}
}
int cal_syst2(){

        TString data = "DoubleMuon";
	run(data,"18");
	run(data,"17");
	run(data,"16pre");
	run(data,"16post");


	return 1;
}
