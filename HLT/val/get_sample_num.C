TH2D*get_hist(TString dir,float xs,TString cut,TString filename,TString year,float lumi){
        TFile*file;TTree*tree;TChain*chain;
	TChain*hist; TObjArray *fileElements;

	chain=new TChain("Events","");
	chain->Add(dir+"/Skim_*.root");
	tree=chain;
	float ntot=0;int i=0;float nevents=0;
	if(filename.Contains("EGamma")==0 && filename.Contains("SinglePhoton")==0 && filename.Contains("Muon")==0 && filename.Contains("Ele")==0 ){
		hist=new TChain("nEventsGenWeighted","");
		hist->Add(dir+"/Skim_*.root");
		fileElements=hist->GetListOfFiles();
		TIter next(fileElements);
		TChainElement *chEl=0;
		while ( ( chEl=(TChainElement*)next() ) ) {
			TFile f=(chEl->GetTitle());
			TH1D*h1=(TH1D*)f.Get("nEventsGenWeighted");
			ntot=ntot+h1->GetSum();
			i++;
		}
		nevents = ntot;
	}

	TString weight;
	if(filename.Contains("EGamma")==0 && filename.Contains("SinglePhoton")==0 && filename.Contains("Muon")==0 && filename.Contains("Ele")==0 ){
		weight=Form("%.8f",1000*xs/nevents*lumi);
		weight=weight+"*vjj_weight*abs(Generator_weight)/Generator_weight";
	}
	else    weight=Form("%.8f",xs);
//	cout<<"Weights are "<<weight<<endl; 
         

	TString histname="hist_"+filename;
	TString hname="h_"+filename;

	vector<float> ptbins={200,220,240,280,320,360,400,450};
	if(year.Contains("16")) ptbins={175,200,220,240,280,320,360,400,450};
        vector<float> etabins={0,0.15,0.3,0.45,0.6,0.75,0.9,1.05,1.2,1.35,1.5,1.7,1.9,2.1,2.3,2.5};

        TH2D*h1=new TH2D(histname,"",ptbins.size()-1,&ptbins[0],etabins.size()-1,&etabins[0]);
        const int nbinsX=h1->GetNbinsX();
	const int nbinsY=h1->GetNbinsY();

	tree->Draw("abs(vjj_v_eta):vjj_v_pt>>"+histname,"("+cut+")*"+weight,"goff");
	for(int i=0;i<nbinsY;i++){
		if(h1->GetBinContent(nbinsX+1,i)>0){
			h1->SetBinContent(nbinsX,i,h1->GetBinContent(nbinsX,i)+h1->GetBinContent(nbinsX+1,i));
			h1->SetBinError(nbinsX,i,sqrt( pow(h1->GetBinError(nbinsX,i),2)+pow(h1->GetBinError(nbinsX+1,i),2) ) );
		}
	}
	cout<<filename<<" number of entries "<<tree->GetEntries()<<", number of yields "<<h1->GetSumOfWeights()<<endl;
	return h1;
}
void run(TString data,TString year,TString sel_cut){
    TH2D*hdata;THStack*hs=new THStack("hs","");
    TString high_cut,low_cut;
    vector<TString>filenames;
    if(data.Contains("EGamma")){
	    if(year.Contains("16")){
		    high_cut= "(vjj_isGood==1 && vjj_jj_m>500 && vjj_v_pt>175 &&  vjj_lead_pt>30 && vjj_sublead_pt>30 && ( (abs(vjj_v_eta)<1.442) || (abs(vjj_v_eta)<2.5 && abs(vjj_v_eta)>1.566) ) && vjj_trig>1 && Flag_goodVertices && Flag_globalSuperTightHalo2016Filter && Flag_HBHENoiseFilter && Flag_HBHENoiseIsoFilter && Flag_EcalDeadCellTriggerPrimitiveFilter && Flag_BadPFMuonFilter && Flag_BadPFMuonDzFilter && Flag_eeBadScFilter && Flag_ecalBadCalibFilter)";
		    low_cut= "(vjj_isGood==1 && vjj_jj_m>500 && abs(vjj_jj_deta)>3 && vjj_v_pt>75 && vjj_v_pt<175 && vjj_lead_pt>30 && vjj_sublead_pt>30 &&  vjj_trig>0  && ( (abs(vjj_v_eta)<1.442) || (abs(vjj_v_eta)<2.5 && abs(vjj_v_eta)>1.566) ) )";
	    }
	    else{
		    high_cut= "(vjj_isGood==1 && vjj_jj_m>500 && vjj_v_pt>200 && vjj_lead_pt>30 && vjj_sublead_pt>30 && ( (abs(vjj_v_eta)<1.442 && vjj_v_pt>200) || ( abs(vjj_v_eta)<2.5 && abs(vjj_v_eta)>1.566 && vjj_v_pt>200 ) ) & vjj_trig>1 && Flag_goodVertices && Flag_globalSuperTightHalo2016Filter && Flag_HBHENoiseFilter && Flag_HBHENoiseIsoFilter && Flag_EcalDeadCellTriggerPrimitiveFilter && Flag_BadPFMuonFilter && Flag_BadPFMuonDzFilter && Flag_eeBadScFilter && Flag_ecalBadCalibFilter)";
		    low_cut= "(vjj_isGood==1 && vjj_jj_m>500 && abs(vjj_jj_deta)>3 && vjj_v_pt<200 && vjj_v_pt>75 && vjj_lead_pt>30 && vjj_sublead_pt>30 &&  vjj_trig>0 && ( (abs(vjj_v_eta)<1.442) || (abs(vjj_v_eta)<2.5 && abs(vjj_v_eta)>1.566) ) )";
	    }
    }
    double lumi;
    if(year=="18") lumi=59.7;
    if(year=="17") lumi=41.5;
    if(year=="16pre") lumi=19.52;
    if(year=="16post") lumi=16.81;
    TString cut1;
    if(sel_cut.Contains("high")) cut1=high_cut;
    if(sel_cut.Contains("low")) cut1=low_cut;
    cout<<"Data: "<<data<<" "<<", the selection is "<<cut1<<endl;
 
    vector<TString> barrel_endcap={"(abs(vjj_v_eta)<1.4442 && vjj_a_hoe <= 0.04596 && vjj_a_sieie <= 0.0133 && (5*1.694 < 0.2*vjj_v_pt ? vjj_a_pfRelIso03_chg*vjj_v_pt < 5*1.694 : vjj_a_pfRelIso03_chg*vjj_v_pt < 0.2*vjj_v_pt))","(abs(vjj_v_eta)>1.566 && abs(vjj_v_eta)<2.5 && vjj_a_hoe <= 0.059 && vjj_a_sieie <= 0.027 && (5*2.089 < 0.2*vjj_v_pt ? vjj_a_pfRelIso03_chg*vjj_v_pt < 5*2.089 : vjj_a_pfRelIso03_chg*vjj_v_pt < 0.2*vjj_v_pt))"};
    vector<TString> pt_range_b;
    if(year.Contains("16")==0) pt_range_b={"(vjj_v_pt>200 && vjj_v_pt<250)","(vjj_v_pt>250&&vjj_v_pt<300)","(vjj_v_pt>300)"};
    else pt_range_b={"(vjj_v_pt>175 && vjj_v_pt<250)","(vjj_v_pt>250&&vjj_v_pt<300)","(vjj_v_pt>300)"};
    vector<TString> pt_range_e;
    if(year.Contains("16")==0) pt_range_e={"(vjj_v_pt>200 && vjj_v_pt<250)","(vjj_v_pt>250)"};
    else pt_range_e={"(vjj_v_pt>175 && vjj_v_pt<250)","(vjj_v_pt>250)"};

    TFile*fout=new TFile("photon_pt_eta_2d_"+sel_cut+year+".root","recreate");
    ifstream file("file_"+data+year);int i=0;
    if(!file.is_open()){cout<<"can not open the file, break the code."<<endl;abort();}
    double count=0;
    TH2D*sumMC;
    TH2D*sumfake;
    while(!file.eof()){
	    TString dir;TString filename;float xs;
	    file>>dir>>xs>>filename;
            filenames.push_back(filename);
	    if(dir.Contains("end")) break;
	    cout<<"process :"<<filename<<endl;
	    if(filename.Contains("EGamma")==0 && filename.Contains("Muon")==0){
                    TString cut = "(" + cut1 + "&& (vjj_photonIsMatched==1) )";
//                    if(filename.Contains("QCD")) cut = "("+cut +"&& vjj_v_pt<250)" ;

		    TH2D*hist = get_hist(dir,xs,cut,filename,year,lumi);
		    if(filename.Contains("EW")) sumMC=(TH2D*)hist->Clone(); 
		    else sumMC->Add(hist);
		    fout->cd();
		    hist->Write(filename);
                    cout<<"################## NEXT ##################"<<endl;
		    hs->Add(hist);
		    cout<<endl;
                    count=count+hist->GetSumOfWeights();
	    }
	    else if(filename.Contains("fake1")){
		    TString cut = "("+ cut1 + "&& (" + pt_range_b[0] + "&&" + barrel_endcap[0] + ") )";
		    TH2D*hist=get_hist(dir,xs,cut,filename,year,1);
                    sumfake=(TH2D*)hist->Clone();
		    fout->cd();
		    hist->Write(filename);
		    hs->Add(hist);
                    count=count+hist->GetSumOfWeights();
	    }
	    else if(filename.Contains("fake2")){
		    TString cut = "("+ cut1 + "&& (" + pt_range_b[1] + "&&" + barrel_endcap[0] + ") )";
		    TH2D*hist=get_hist(dir,xs,cut,filename,year,1);
		    fout->cd();
		    hist->Write(filename);
		    hs->Add(hist);
		    sumfake->Add(hist);
                    count=count+hist->GetSumOfWeights();
	    }
	    else if(filename.Contains("fake3")){
		    TString cut = "("+ cut1 + "&& (" + pt_range_b[2] + "&&" + barrel_endcap[0] + ") )";
		    TH2D*hist=get_hist(dir,xs,cut,filename,year,1);
		    fout->cd();
		    hist->Write(filename);
		    hs->Add(hist);
		    sumfake->Add(hist);
                    count=count+hist->GetSumOfWeights();
	    }
	    else if(filename.Contains("fake4")){
		    TString cut = "("+ cut1 + "&& (" + pt_range_e[0] + "&&" + barrel_endcap[1] + ") )";
		    TH2D*hist=get_hist(dir,xs,cut,filename,year,1);
		    fout->cd();
		    hist->Write(filename);
		    hs->Add(hist);
		    sumfake->Add(hist);
                    count=count+hist->GetSumOfWeights();
	    }
	    else if(filename.Contains("fake5")){
		    TString cut = "("+ cut1 + "&& (" + pt_range_e[1] + "&&" + barrel_endcap[1] + ") )";
		    TH2D*hist=get_hist(dir,xs,cut,filename,year,1);
		    fout->cd();
		    hist->Write(filename);
		    hs->Add(hist);
		    sumfake->Add(hist);
                    count=count+hist->GetSumOfWeights();
	    }
	    else{
		    hdata=get_hist(dir,xs,cut1,filename,year,1);
		    fout->cd();
		    hdata->Write(filename);
	    }
	    if(filename.Contains("fake")==0) i++;
    }
    sumMC->Write("sumMC");
    sumfake->Write("sumfake");
    hs->Write();
    cout<<"Data: "<<hdata->GetSumOfWeights()<<endl;
    cout<<"Pred.: "<<count<<endl;
    file.close();
    fout->Close();
}
int get_sample_num(){
	vector<TString> data={"EGamma"};
//	run(data[0],"18","high");
//	run(data[0],"17","high");
	run(data[0],"16pre","high");
	run(data[0],"16post","high");

	return 1;
}
